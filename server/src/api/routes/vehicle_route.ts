import { Vehicle } from '@/interfaces/vehicle';
import VehicleService from '@/services/vehicleService';
import { Joi, celebrate } from 'celebrate';
import { Router, Request, Response } from 'express';
import Container from 'typedi';
const route = Router();

export default (app: Router) => {
  app.use('/vehicle', route);

  route.get('/get_vehicle_list', celebrate({
    query: Joi.object().keys({
      vehicleType: Joi.number().required()
    })
  }), async (req: Request, res: Response) => {
    const result: { message: string | Vehicle[], flag: boolean } = await Container
      .get(VehicleService).vehicleList(req, res);
    if (result.flag) {
      return res.status(200).json({ success: true, result: { message: result.message } });
    } else {
      return res.status(200).json({ success: false, result: { error: result.message } });
    }
  });
};
