import { BookingStruct } from '@/interfaces/booking';
import BookingService from '@/services/bookingService';
import { Joi, celebrate } from 'celebrate';
import { Router, Request, Response } from 'express';
import Container from 'typedi';
const route = Router();

export default (app: Router) => {
    app.use('/booking', route);

    route.post('/book_vehicle', celebrate({
        body: Joi.object().keys({
            firstName: Joi.string().required(),
            lastName: Joi.string().required(),
            vehicleId: Joi.number().required(),
            startDate: Joi.date().required(),
            endDate: Joi.date().required()
        })
    }), async (req: Request, res: Response) => {
        const result: { message: string | BookingStruct, flag: boolean } = await Container
            .get(BookingService).BookVehicle(req, res);
        if (result.flag) {
            return res.status(200).json({ success: true, result: { message: result.message } });
        } else {
            return res.status(200).json({ success: false, result: { error: result.message } });
        }
    });
};