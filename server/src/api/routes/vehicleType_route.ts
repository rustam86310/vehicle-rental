import { VehicleType } from '@/interfaces/vehicle';
import VehicleService from '@/services/vehicleService';
import VehicleTypeService from '@/services/vehicleTypeService';
import { Joi, celebrate } from 'celebrate';
import { Router, Request, Response } from 'express';
import Container from 'typedi';
const route = Router();

export default (app: Router) => {
  app.use('/vehicle_type', route);

  route.get('/get_list', celebrate({
    query: Joi.object().keys({
      numberOfWheels: Joi.number().required()
    })
  }), async (req: Request, res: Response) => {
    const result: { message: string | VehicleType[], flag: boolean } = await Container
      .get(VehicleTypeService).getVehicleTypeList(req, res);
    if (result.flag) {
      return res.status(200).json({ success: true, result: { message: result.message } });
    } else {
      return res.status(200).json({ success: false, result: { error: result.message } });
    }
  });
};
