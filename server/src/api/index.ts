import { Router } from 'express';
import vehicleType_route from './routes/vehicleType_route';
import vehicle_route from './routes/vehicle_route';
import booking_route from './routes/booking_route';

export default () => {
	const app = Router();
	vehicle_route(app);
	vehicleType_route(app);
	booking_route(app);

	return app
}