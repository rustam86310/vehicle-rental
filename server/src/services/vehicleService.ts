import { Vehicle } from '@/interfaces/vehicle';
import { Request, Response } from 'express';
import { Service, Inject } from 'typedi';

@Service()
export default class VehicleService {
  constructor(
    @Inject('Vehicle') private VehicleModel,
    @Inject('logger') private logger,
  ) {
  }

  public async vehicleList(req: Request, res: Response)
  : Promise<{ message: string | Vehicle[], flag: boolean }> {
    try {
      const { vehicleType } = req.query;

      let entry = await this.VehicleModel.findAll({where: {vehicleType: vehicleType}});
      if (entry?.length === 0) {
        return { message: "No vehicle found.", flag: false };
      }

      return { message: entry, flag: true };
    } catch (e) {
      this.logger.error(e.message);
      return { message: e.message, flag: false };
    }
  }
}
