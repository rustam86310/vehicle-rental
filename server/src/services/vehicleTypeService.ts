import { VehicleType, VehicleTypeName } from '@/interfaces/vehicle';
import { Request, Response } from 'express';
import { Service, Inject } from 'typedi';

@Service()
export default class VehicleTypeService {
  constructor(
    @Inject('VehicleType') private VehicleTypeModel,
    @Inject('logger') private logger,
  ) {
  }

  public async getVehicleTypeList(req: Request, res: Response)
    : Promise<{ message: string | VehicleType[], flag: boolean }> {
    try {
      const { numberOfWheels } = req.query;
      let name = '';
      if (+numberOfWheels === 2) {
        name = VehicleTypeName.BIKE;
      } else if (+numberOfWheels === 4) {
        name = VehicleTypeName.CAR;
      }

      let entry = await this.VehicleTypeModel.findAll({where: { name: name }});
      if (entry?.length === 0) {
        return { message: "Vehicle of this type not available.", flag: false };
      }

      return { message: entry, flag: true };
    } catch (e) {
      this.logger.error(e.message);
      return { message: e.message, flag: false };
    }
  }
}
