import { BookingStruct } from '@/interfaces/booking';
import { Request, Response } from 'express';
import { Op } from 'sequelize';
import { Service, Inject } from 'typedi';

@Service()
export default class BookingService {
  constructor(
    @Inject('Booking') private BookingModel,
    @Inject('logger') private logger,
  ) {
  }

  public async BookVehicle(req: Request, res: Response)
    : Promise<{ message: string | BookingStruct, flag: boolean }> {
    try {
      const { firstName, lastName, vehicleId, startDate, endDate } = req.body;

      if (new Date(startDate) > new Date(endDate)) {
        return { message: "Invalid date range.", flag: false };
      }

      let bookedVehicle = await this.BookingModel.findOne({
        where: {
          vehicleId: vehicleId,
          [Op.or]: [
            {
              startDate: { [Op.lte]: endDate },
              endDate: { [Op.gte]: startDate }
            }
          ]
        }
      });
      if (bookedVehicle) {
        return { message: "Vehicle not available on given date range.", flag: false };
      }

      const booking: BookingStruct = {
        firstName,
        lastName,
        vehicleId,
        startDate,
        endDate,
        totalAmount: 0,
        createdAt: new Date(),
        updatedAt: new Date()
      };

      const entry = await this.BookingModel.create(booking);
      if (!entry) {
        return { message: "Failed to book vehicle.", flag: false };
      }

      return { message: entry, flag: true };
    } catch (e) {
      this.logger.error(e.message);
      return { message: e.message, flag: false };
    }
  }
}
