import express from 'express';
import cors from 'cors';
import routes from '@/api';
import config from '@/config';
import { isCelebrateError } from 'celebrate';
export default ({ app }: { app: express.Application }) => {
  // The magic package that prevents frontend developers going nuts
  // Alternate description:
  // Enable Cross Origin Resource Sharing to all origins by default
  app.use(cors());
  // Transforms the raw string of req.body into json
  app.use(express.json());
  // Load API routes
  app.use(config.api.prefix, routes());

  /// catch 404 and forward to error handler
  app.use((req, res, next) => {
    const err = new Error('Not Found');
    err['status'] = 404;
    next(err);
  });

  app.use((err, req, res, next) => {
    if (err.name === 'UnauthorizedError') {
      return res
        .status(err.status)
        .send({ message: err.message })
        .end();
    }
    return next(err);
  });
  app.use((err, req, res, next) => {
    let message = err.message;
    if (isCelebrateError(err)) {
      if (err.details.get('body')) {
        message = err.details.get('body').message;
      }
      if (err.details.get('params')) {
        message = err.details.get('params').message;
      }
      if (err.details.get('query')) {
        message = err.details.get('query').message;
      }
    }
    res.status(err.status || 200);
    return res.json({ success: false, result: { error: message } });
  });
};
