import expressLoader from './express';
import dependencyInjectorLoader from './dependencyInjector';
import sequelizeLoader from './sequelize';
import Logger from './logger';

export default async ({ expressApp }) => {
  await sequelizeLoader();
  Logger.info('✌️ DB loaded and connected!');

  await dependencyInjectorLoader();
  Logger.info('✌️ Dependency Injector loaded');

  await expressLoader({ app: expressApp });
  Logger.info('✌️ Express loaded');
};
