import { Container } from 'typedi';
import dbConfig from '../models';
import LoggerInstance from './logger';

export default async () => {
  const models = dbConfig;
  const db = Object.keys(models);
  db.forEach((el) => {
    Container.set(el, models[el]);
  });

  Container.set('logger', LoggerInstance);
};
