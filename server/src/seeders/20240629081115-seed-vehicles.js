'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    // Get vehicle type IDs
    const vehicleTypes = await queryInterface.sequelize.query(
      `SELECT id, name from VehicleTypes;`
    );

    const vehicleTypeRows = vehicleTypes[0];

    await queryInterface.bulkInsert('Vehicles', [
      { name: 'cruiser', vehicleType: vehicleTypeRows.find(type => type.name === 'bike').id, vehicleModel: 'sports', rentalRate: 100, createdAt: new Date(), updatedAt: new Date() },
      { name: 'sports', vehicleType: vehicleTypeRows.find(type => type.name === 'bike').id, vehicleModel: 'sports', rentalRate: 150, createdAt: new Date(), updatedAt: new Date() },
      { name: 'hatchback', vehicleType: vehicleTypeRows.find(type => type.name === 'car').id, vehicleModel: 'hatchback', rentalRate: 80, createdAt: new Date(), updatedAt: new Date() },
      { name: 'suv', vehicleType: vehicleTypeRows.find(type => type.name === 'car').id, vehicleModel: 'suv', rentalRate: 200, createdAt: new Date(), updatedAt: new Date() },
      { name: 'sedan', vehicleType: vehicleTypeRows.find(type => type.name === 'car').id, vehicleModel: 'sedan', rentalRate: 180, createdAt: new Date(), updatedAt: new Date() }
    ], {});
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('Vehicles', null, {});
  }
};
