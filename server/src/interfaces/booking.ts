export interface BookingStruct {
    id?: number;
    firstName: string;
    lastName: string;
    vehicleId: number;
    startDate: Date;
    endDate: Date;
    totalAmount: number;
    createdAt?: Date;
    updatedAt?: Date;
}