export interface Vehicle {
  id: number;
  name: string;
  vehicleType: number;
  vehicleModel: string;
  rentalRate: number;
  createdAt?: Date;
  updatedAt?: Date;
}

export interface VehicleType {
  id: number;
  name: string;
}

export enum VehicleTypeName {
  CAR = 'car',
  BIKE = 'bike',
}