
import { VehicleType } from '@/interfaces/vehicle';
import { Model, DataTypes, Sequelize } from 'sequelize';

module.exports = (sequelize: Sequelize) => {
  class VehicleTypeModel extends Model<VehicleType> implements VehicleType {
    id: number;
    name: string;

    static associate: (models: any) => void;
  }

  VehicleTypeModel.init(
    {
      id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
      },
      name: {
        type: DataTypes.STRING,
        allowNull: false,
      },
    },
    {
      sequelize,
      modelName: 'VehicleType',
    }
  );

  VehicleTypeModel.associate = function (models: any) {

  };

  return VehicleTypeModel;
};
