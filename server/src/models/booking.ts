import { BookingStruct } from '@/interfaces/booking';
import { Model, DataTypes, Sequelize } from 'sequelize';

module.exports = (sequelize: Sequelize) => {
  class BookingModel extends Model<BookingStruct> implements BookingStruct {
    id: number;
    firstName: string;
    lastName: string;
    vehicleId: number;
    startDate: Date;
    endDate: Date;
    totalAmount: number;
    createdAt: Date;
    updatedAt: Date;

    static associate: (models: any) => void;
  }

  BookingModel.init(
    {
      id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
      },
      firstName: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      lastName: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      vehicleId: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
      startDate: {
        type: DataTypes.DATE,
        defaultValue: 0,
      },
      endDate: {
        type: DataTypes.DATE,
        defaultValue: 0,
      },
      totalAmount: {
        type: DataTypes.INTEGER,
        defaultValue: 0,
      },
      createdAt: {
        type: DataTypes.DATE,
        allowNull: false,
      },
      updatedAt: {
        type: DataTypes.DATE,
        allowNull: false,
      },
    },
    {
      sequelize,
      modelName: 'Booking',
    }
  );

  BookingModel.associate = function (models: any) {
    BookingModel.belongsTo(models.Vehicle, { foreignKey: 'vehicleId' });
  };

  return BookingModel;
};
