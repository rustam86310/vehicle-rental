import { Vehicle } from '@/interfaces/vehicle';
import { Model, DataTypes, Sequelize } from 'sequelize';

module.exports = (sequelize: Sequelize) => {
  class VehicleModel extends Model<Vehicle> implements Vehicle {
    id: number;
    name: string;
    vehicleType: number;
    vehicleModel: string;
    rentalRate: number;
    createdAt: Date;
    updatedAt: Date;

    static associate: (models: any) => void;
  }

  VehicleModel.init(
    {
      id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
      },
      name: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      vehicleType: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
      vehicleModel: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      rentalRate: {
        type: DataTypes.INTEGER,
        defaultValue: 0,
      },
      createdAt: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
      },
      updatedAt: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'),
      },
    },
    {
      sequelize,
      modelName: 'Vehicle',
    }
  );

  VehicleModel.associate = function (models: any) {
    VehicleModel.belongsTo(models.VehicleType, { foreignKey: 'vehicleType' });
  };

  return VehicleModel;
};
