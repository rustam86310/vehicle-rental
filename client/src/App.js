import Home from "./pages/home/home";
import { BrowserRouter, Route, Routes } from 'react-router-dom'
import VehicleRental from "./pages/vehicleRental/vehicleRental";

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/vehicle-rental" element={<VehicleRental />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
