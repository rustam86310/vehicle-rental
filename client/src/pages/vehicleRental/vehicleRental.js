import React, { useState } from 'react';
import {
    Box, Button, TextField, RadioGroup, FormControlLabel, Radio, Typography, Snackbar, Alert,
    Grid
} from '@mui/material';
import apiService from '../../services/apiService';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

const initialState = {
    firstName: '',
    lastName: '',
    wheels: '',
    vehicleType: '',
    specificModel: '',
    startDate: '',
    endDate: '',
};

const questions = [
    { type: 'name', label: 'What is your name?', inputs: ['First Name', 'Last Name'] },
    { type: 'wheels', label: 'Number of wheels', options: ['2', '4'] },
    { type: 'vehicleType', label: 'Type of vehicle', options: [] },
    { type: 'specificModel', label: 'Specific Model', options: [] },
    { type: 'date', label: 'Date range picker', options: ['Start Date', 'End Date'] },
];

const VehicleRental = () => {

    const [currentQuestion, setCurrentQuestion] = useState(0);
    const [formData, setFormData] = useState(initialState);
    const [vehicleTypes, setVehicleTypes] = useState([]);
    const [vehicles, setVehicles] = useState([]);
    const [error, setError] = useState('');
    const [success, setSuccess] = useState('');

    const handleNext = async () => {
        const { firstName, lastName, wheels, vehicleType, specificModel, startDate, endDate } = formData;

        if (currentQuestion === 0 && (!firstName || !lastName)) {
            setError('Please enter both first and last name.');
            return;
        }
        if (currentQuestion === 1) {
            if (!wheels) {
                setError('Please select the number of wheels.');
                return;
            } else {
                let res = await getVehicleType();
                if (!res) return;
            }
        }
        if (currentQuestion === 2) {
            if (!vehicleType) {
                setError('Please select a vehicle type.');
                return;
            } else {
                let res = await getVehicles();
                if (!res) return;
            }
        }
        if (currentQuestion === 3 && !specificModel) {
            setError('Please select a specific model.');
            return;
        }
        if (currentQuestion === 4 && !startDate) {
            setError('Please select a date range.');
            return;
        }
        if (currentQuestion === 4 && !endDate) {
            setError('Please select a date range.');
            return;
        }

        setError('');
        if (currentQuestion < questions.length - 1) {
            setCurrentQuestion(currentQuestion + 1);
        } else {
            finalSubmit();
        }
    };

    const handleInputChange = (e) => {
        const { name, value } = e.target;
        setFormData({
            ...formData,
            [name]: value
        });
    };

    const handleDate = (data, name) => {
        setFormData({
            ...formData,
            [name]: data
        });
    }

    const getVehicleType = async () => {
        let data = await apiService.request('GET', 'vehicle_type/get_list', null, { numberOfWheels: formData.wheels });
        if (data.success) {
            setVehicleTypes(data.result.message);
        } else {
            setError(data.result.error);
            return false;
        }
        return true;
    }

    const getVehicles = async () => {
        let data = await apiService.request('GET', 'vehicle/get_vehicle_list', null, { vehicleType: formData.vehicleType });
        if (data.success) {
            setVehicles(data.result.message);
        } else {
            setError(data.result.error);
            return false;
        }
        return true;
    }

    const finalSubmit = async () => {
        const { firstName, lastName, wheels, vehicleType, specificModel, startDate, endDate } = formData;
        let obj = {
            firstName: firstName,
            lastName: lastName,
            vehicleId: specificModel,
            startDate: startDate,
            endDate: endDate
        }
        let data = await apiService.request('POST', 'booking/book_vehicle', obj);
        if (data.success) {
            setSuccess("Vehicle Booked successfully.");
            setFormData(initialState);
            setCurrentQuestion(0);
        } else {
            setError(data.result.error);
            return false;
        }
        return true;
    }

    return (
        <Box sx={{ p: 2, maxWidth: 500, mx: 'auto' }}>
            <Typography variant="h5">{questions[currentQuestion].label}</Typography>
            <Box sx={{ mt: 2 }}>
                {questions[currentQuestion].type === 'name' && (
                    <Box>
                        {questions[currentQuestion].inputs.map((input, idx) => (
                            <TextField
                                key={idx}
                                label={input}
                                name={input.includes('First') ? 'firstName' : 'lastName'}
                                value={formData[input]}
                                onChange={handleInputChange}
                                fullWidth
                                margin="normal"
                            />
                        ))}
                    </Box>
                )}
                {questions[currentQuestion].type === 'wheels' && (
                    <RadioGroup
                        name="wheels"
                        value={formData.wheels}
                        onChange={handleInputChange}
                    >
                        {questions[currentQuestion].options.map((option, idx) => (
                            <FormControlLabel key={idx} value={option} control={<Radio />} label={option} />
                        ))}
                    </RadioGroup>
                )}
                {questions[currentQuestion].type === 'vehicleType' && (
                    <RadioGroup
                        name="vehicleType"
                        value={formData.vehicleType}
                        onChange={handleInputChange}
                    >
                        {vehicleTypes?.map((option, idx) => (
                            <FormControlLabel key={idx} value={option.id} control={<Radio />} label={option.name} />
                        ))}
                    </RadioGroup>
                )}
                {questions[currentQuestion].type === 'specificModel' && (
                    <RadioGroup
                        name="specificModel"
                        value={formData.specificModel}
                        onChange={handleInputChange}
                    >
                        {vehicles?.map((option, idx) => (
                            <FormControlLabel key={idx} value={option.id} control={<Radio />} label={option.name} />
                        ))}
                    </RadioGroup>
                )}
                {questions[currentQuestion].type === 'date' && (
                    <Box p={2}>
                        <Grid container spacing={2}>
                            {questions[currentQuestion].options.map((option, idx) => (
                                <Grid item xs={12} sm={6} key={idx}>
                                    <DatePicker
                                        label={option}
                                        fullWidth
                                        margin="normal"
                                        key={idx}
                                        placeholderText={option}
                                        selected={formData[option.includes('Start') ? 'startDate' : 'endDate']}
                                        name={option.includes('Start') ? 'startDate' : 'endDate'}
                                        onChange={(date) => handleDate(date, option.includes('Start') ? 'startDate' : 'endDate')}
                                    />
                                </Grid>
                            ))}
                        </Grid>
                    </Box>
                )}
            </Box>
            <Button variant="contained" onClick={handleNext} sx={{ mt: 2, py: 1 }} fullWidth>
                Next
            </Button>
            <Snackbar open={!!error} autoHideDuration={5000} onClose={() => setError('')}>
                <Alert onClose={() => setError('')} severity="error" sx={{ width: '100%' }}>
                    {error}
                </Alert>
            </Snackbar>
            <Snackbar open={!!success} autoHideDuration={5000} onClose={() => setSuccess('')}>
                <Alert onClose={() => setSuccess('')} severity="success" sx={{ width: '100%' }}>
                    {success}
                </Alert>
            </Snackbar>
        </Box>
    );
};

export default VehicleRental;