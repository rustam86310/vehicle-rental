import { Link } from "react-router-dom";

const Home = () => {
    return (
        <div className="main-section">
            <h1>Welcome to the Vehicle Rental </h1>
            <p>
                Book Your vehicle <Link className="btn btn-success" to={'/vehicle-rental'}> now</Link>
            </p>
            
        </div>
    );
};

export default Home;